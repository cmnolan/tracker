# Copyright Linde 2017
# Author: Paul Stanley

import time
import machine

from machine import Pin,Timer

def proc_int(alarm):
    if alarm.pin() == alarm.level:
        alarm.trigger()
    else:
        alarm.clear()

# for alarms that exist if they are retriggered within a specific time and
# are only cleared if not retriggered - we use a timer not an edge to clear
def proc_intexp(alarm):
    if not alarm.delay:
        alarm.trigger()
        alarm.timeout = alarm.expiry # reset the time to clear
    else:
        now = time.time()
        alarm.timeout = alarm.expiry # reset the time to clear
        if now - alarm.trigTime >= alarm.delay:
            alarm.edgeCount += 1 # as a further protection against noise we check for at
            if (alarm.edgeCount >= alarm.delay): # least one trigger per second during delay
                alarm.trigger()
                alarm.surpressed = 0 # we obviously didn't surpress
        elif now-alarm.trigTime > 0:
            alarm.edgeCount += 1
            return  # we're in the possible surpression period
        else:
            alarm.trigTime   = now # we triggered - need to wait delay time
            alarm.surpressed = 1 # we may surpress this so track for reporting
            alarm.edgeCount = 1

# check if the expiry time has elapsed
def checkStatus(alarm):
    if alarm.status:
        intmask = machine.disable_irq()
        alarm.timeout = alarm.timeout-1
        if alarm.timeout <= 0:
            alarm.timeout=0
            alarm.clear()
        machine.enable_irq(intmask)
    if (alarm.trigTime < 0x7FFFFFFF) and (time.time() > (alarm.trigTime + alarm.delay + 2)):
        alarm.trigTime   = 0x7FFFFFFF # reset our trigger if we're 2 seconds past our window


# this alarm class can be used as a internal/SW alarm or as a IO generated alarm

class GenAlarm:

    def __init__(self, pin=None, level=None, expiry=0, delay = 0):
        self.count       = 0
        self.total_dur   = 0
        self.last_dur    = 0
        self.triggered   = 0
        self.cleared     = 0
        self.trig_secs   = 0
        self.status      = 0
        # if we have a time based triggering or clearing we need a timer and tracking variables
        self.timer       = 0 # Thise have a time based triggering we need a timer
        self.expiry      = expiry # no. of secs of no signal before clearing
        self.delay       = delay  # no. of secs of signal before triggering to surpress noise
        self.surpressed  = 0 # a flag to indicate we have ignored a possible trigger
        self.trigTime    = 0x7FFFFFFF # a time in the distant future => no trigger
        self.edgeCount   = 0 # count of edges to help surpress noise      
        if level == None:
            self.level = 1   # default high voltage on the pin is an alarm
        else:
            self.level = level
        if not expiry:  # the alarm will be trigger/cleared by a signal edge
            if not pin == None:
                self.pin = Pin(pin, mode=Pin.IN, )
                self.pin.callback(Pin.IRQ_RISING | Pin.IRQ_FALLING, proc_int, self)
        else: # the alarm will be cleared if not retriggered in the expiry time (secs)
            if not pin == None:
                self.pin = Pin(pin, mode=Pin.IN, pull=Pin.PULL_UP)
                # we can trigger on either edge
                self.pin.callback(Pin.IRQ_RISING , proc_intexp, self)
                self.timeout = 0
                self.timer = Timer.Alarm(checkStatus, s=1, arg=self, periodic=True)


    def trigger(self):
        if self.status == 0: # ony trigger if we're not already on
            self.trig_secs = time.time()
            self.status    = 1
            self.triggered = 1
            self.last_dur  = 0
            return True
        else:
            return False

    # once we have processed the new alarm we'll want to ack it
    def ack_trigger(self):  
        if self.triggered:
            self.triggered = 0
            return True
        else:
            return False

    def clear(self):
        if self.status == 1: # only clear if we're not already off
            self.cleared    = 1
            self.status     = 0
            self.count     += 1 # only count when alarm is over
            self.last_dur   = time.time() - self.trig_secs
            self.total_dur += self.last_dur
            return True
        else:
            return False

    # once we have processed the alarm going away we'll want to ack it
    def ack_clear(self):  
        if self.cleared:
            self.cleared = 0
            return True
        else:
            return False

    # reset alarm counters
    def reset(self):
        self.count     = 0
        self.total_dur = 0

    # turn off interrupt handlers
    def disable(self):
        self.pin.callback(Pin.IRQ_RISING | Pin.IRQ_FALLING, handler=None)
        if self.timer:
            self.timer.cancel()


