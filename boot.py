# boot.py -- run on boot-up
import os
import machine
import network

from machine import UART
from network import WLAN

uart = UART(0, 115200)
os.dupterm(uart)
wlan = WLAN() # get current object, without changing the mode
wlan.deinit() # save power on startup/wakeup by default

server = network.Server()
server.deinit() # disable the server
server.init(login=('micro', 'btf987'), timeout=6000)


#if machine.reset_cause() != machine.SOFT_RESET:
#    wlan.init(mode=WLAN.STA)
#    # configuration below MUST match your home router settings!!
#    wlan.ifconfig(config=('dhcp'))
#
#if not wlan.isconnected():
#    # change the line below to match your network ssid, security and password
#    wlan.connect('Transfer', auth=(WLAN.WPA2, 'b2tf1916'), timeout=5000)
#    # don't wait for wifi
