#
#   Copyright Linde 2017
#   Author: Paul Stanley
#

class conf:

    VERSION        = 1         # S/W Version no.

    LOGLEVEL       = 3         # 0-> errors, 1 -> useful info, 2 -> continuous status, 3 -> verbose
    MAXLOGLEN      = 1024000  # limit logfiles to  < 1MB
    LOGFILE        = '/sd/logfile'

    BUTTON_PIN     = 'P10'     # expansion board button
    SDA_PIN        = 'P22'     # expansion board button
    SCL_PIN        = 'P21'     # expansion board button
    
    BATT_CRITICAL  = 0xC0      # to be calibrated - going to run out of battery power
    BATT_OKAY      = 220       # to be calibrated - battery no longer critical
    BATT_CHARGED   = 250       # to be calibrated - battery charged
    BATT_MAX       = 5.0       # max voltage returned by pysense for battery
    
    VLONG_PRESS    = 5000      # msec for a very long press
    LONG_PRESS     = 2000      # msec for a long press
    DEBOUNCE       = 50        # msec debounce for buttons

    HEARTBEAT_FREQ = 24*60*60  # seconds between sending heartbeat messages
    POSMSG_FREQ    = 60*60*4   # Gap in seconds between tracking messageage (normal case)
    MIN_POSMSG_FREQ= 10        # min no. of seconds to sleep (if OTA the freq is set to 0)
    MORNING_START  = 7         # don't send anything from midnight to 7am
    MAX_LOCK_TRYS  = 20        # must be < 32
    START_LOCK_TRYS= 40        # 
    LOCK_TRY_WAIT  = 3         # Seconds between trys
    WDT_TIMEOUT    = 180000    # Seconds for watchdog timeout > max(MAX_LOCK_TRYS,START_LOCK_TRYS)*LOCK_TRY_WAIT

    CYCLE_LED      = 0x003F00  # green led for cycle
    START_LED      = 0x3F0000  # red led for start


