from machine import Timer
import time
import struct
import re

from machine import I2C
from machine import RTC



class L76Pos:

    GPS_I2CADDR = const(0x10)

    def __init__(self, pytrack=None, sda='P22', scl='P21', timeout=None):
        self.lastMessage = ''
        self.lat         = 0 # latitude as a float
        self.lng         = 0 # longitude as a float
        self.latPack     = b'\0\0\0\0' # latitude as 32 bits
        self.lngPack     = b'\0\0\0\0' # longitude as 32 bits
        self.valid       = False
        self.trys        = 0 # no. of the attempt that was successful
        self.rtc         = RTC()
        self.timesync    = False

        if pytrack is not None:
            self.i2c = pytrack.i2c
        else:
            self.i2c = I2C(0, mode=I2C.MASTER, pins=(sda, scl))
        self.chrono = Timer.Chrono()

        self.timeout = timeout
        self.timeout_status = True

        self.reg = bytearray(1)
        self.i2c.writeto(GPS_I2CADDR, self.reg)

    def read(self, length=256):
        try:
            self.lastMessage = self.i2c.readfrom(GPS_I2CADDR, 256)
        except Exception as e:
            self.lastMessage = 'Error reading I2C'

    def syncTime(self,time,date=0):
        try:
            (year, mon, day, hours, mins, secs, msec, tz) = self.rtc.now()
            hours=int(time[0:2])
            mins =int(time[2:4])
            secs =int(time[4:6])
            if date:
               year = int(date[4:6])+2000
               mon  = int(date[2:4])
               day  = int(date[0:2])
            self.rtc.init((year,mon,day,hours,mins,secs))
            self.timesync = True
        except Exception as e: # use this to catch incomplete/corrupt data
            pass


    def position(self, trys=1, wait=0.1, length=256):

        self.valid=False # only reset validity to preserve previous last known position
        self.trys=0
        for attempt in range(1,trys+1):
            try:
                self.read(length)
                messStr=str(self.lastMessage)
                start=messStr.find('RMC,')
                if start == -1:
                    start=messStr.find('GLL,')
                if start == -1:
                    start=messStr.find('GGA,')
                if (start > 0) and (start < length-46):
                    fields = messStr[start:].split(',')
                    if ( fields[0] == 'RMC' ):
                        posstart=3
                        self.valid=fields[2]=='A'
                        if self.valid:
                            self.syncTime(fields[1],fields[9])
                    elif ( fields[0] == 'GLL' ):
                        posstart=1
                        self.valid=fields[6]=='A'
                        if self.valid:
                            self.syncTime(fields[7])
                    elif ( fields[0] == 'GGA' ):
                        posstart=2
                        self.valid=fields[6]!='0'
                        if self.valid:
                            self.syncTime(fields[1])
            
                    if self.valid:
                       self.trys = attempt
                       latstr=fields[posstart]
                       self.lat = int(latstr[0:2]) + float(latstr[2:8])/60
                       if fields[posstart+1]=='S':
                           self.lat=-self.lat
                       lngstr=fields[posstart+2]
                       self.lng = int(lngstr[0:3]) + float(lngstr[3:8])/60
                       if fields[posstart+3]=='W':
                           self.lng=-self.lng
                       self.latPack = struct.pack('<f',self.lat)
                       self.lngPack = struct.pack('<f',self.lng)
                       break

                time.sleep(wait)
    
            except Exception as e:
               self.lastMessage = 'Could not parse GPS message'
               pass # assuming corrupted data so ignore should check better what happened

        return self.trys


    # keep the following for compatibility
    def _read(self):
        self.reg = self.i2c.readfrom(GPS_I2CADDR, 64)
        return self.reg

    def _convert_coords(self, gngll_s):
        lat = gngll_s[1]
        lat_d = (float(lat) // 100) + ((float(lat) % 100) / 60)
        lon = gngll_s[3]
        lon_d = (float(lon) // 100) + ((float(lon) % 100) / 60)
        if gngll_s[2] == 'S':
            lat_d *= -1
        if gngll_s[4] == 'W':
            lon_d *= -1
        return(lat_d, lon_d)

    def coordinates(self, debug=False):
        lat_d, lon_d, debug_timeout = None, None, False
        if self.timeout != None:
            self.chrono.start()
        nmea = b''
        while True:
            if self.timeout != None and self.chrono.read() >= self.timeout:
                self.chrono.stop()
                chrono_timeout = self.chrono.read()
                self.chrono.reset()
                self.timeout_status = False
                debug_timeout = True
            if self.timeout_status != True:
                gc.collect()
                break
            nmea += self._read().lstrip(b'\n\n').rstrip(b'\n\n')
            gngll_idx = nmea.find(b'GNGLL')
            if gngll_idx >= 0:
                gngll = nmea[gngll_idx:]
                e_idx = gngll.find(b'\r\n')
                if e_idx >= 0:
                    try:
                        gngll = gngll[:e_idx].decode('ascii')
                        gngll_s = gngll.split(',')
                        lat_d, lon_d = self._convert_coords(gngll_s)
                    except Exception:
                        pass
                    finally:
                        nmea = nmea[(gngll_idx + e_idx):]
                        gc.collect()
                        break
            else:
                gc.collect()
                if len(nmea) > 4096:
                    nmea = b''
            time.sleep(0.1)
        self.timeout_status = True
        if debug and debug_timeout:
            print('GPS timed out after %f seconds' % (chrono_timeout))
            return(None, None)
        else:
            return(lat_d, lon_d)
