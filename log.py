# Copyright Linde 2017
# Author: Paul Stanley

import time

# this logger class prints to console and writes to a log file if it exists

# these don't work as class methods for some reason
def fileWriteln(self, strout=0):
    if strout:
        fileWrite(self, strout)
    fileWrite(self, '\n')

def fileWrite(self, strout):
    try:
        self.length += self.logfile.write(strout)
        if self.alarm:
            self.alarm.clear()
    except OSError as err:
            self.alarm.trigger()

class Logger:

    def __init__(self, level = 0, path = 0, alarm = 0):
        self.path    = path
        self.level   = level
        self.length  = 0
        self.logfile = 0
        self.alarm   = alarm
        if path:
            try:
                t = time.localtime()
                strg = "{:s}_{:d}-{:02d}-{:02d}_{:02d}{:02d}".format(path,t[0],t[1],t[2],t[3],t[4])
                self.logfile = open(strg,'a')
                self.log (0, "started logfile after boot:" + strg)
            except OSError as err:
                self.logfile = 0
                print ("ERROR opening logfile:",strg)


    def timestamp(self, fileonly = 0):
        t = time.localtime()
        timestmp ="{:d}-{:02d}-{:02d}-{:02d}:{:02d}:{:02d} ".format(t[0],t[1],t[2],t[3],t[4],t[5])
        if not fileonly:
            print(timestmp, end='')
        if self.logfile:
            result = fileWrite(self,timestmp)

    # generic logger to log a string and some values - only the first can be a float
    def log(self, loglevel, logstring, *vals):
        if self.level >= loglevel: 
            self.timestamp()
            valstring = ''.join('{}, '.format(x) for x in vals)
            strout = logstring + " - " + valstring
            print (strout)
            if self.logfile:
                fileWriteln(self,strout)
                if (loglevel == 0):
                    self.flush() # make sure critical messages get to log file

    #special logger to log a string plus message
    def logmess(self, loglevel, logstring, msg):
        if self.level >= loglevel: 
            self.timestamp()
            valstring = ''.join('{:02X}, '.format(x) for x in msg[2:])
            strout = logstring + " - " + '{:2d} '.format(msg[0]) + '{:08b} '.format(msg[1]) + valstring
            print (strout)
            if self.logfile:
                fileWriteln(self,strout)

    #special logger to log adc samples - only to a logfile
    def logadc(self, loglevel, logstring, samples):
        if (self.level >= loglevel) and self.logfile: 
            self.timestamp(1)
            strout = logstring + " - " 
            self.length += self.logfile.write(strout)
            for val in samples:
                strout = '{:d}, '.format(val)
                fileWrite(self,strout)
            fileWriteln(self)

    def flush(self):
        if self.logfile:
            self.logfile.flush()

    def rotatefile(self):
        if self.logfile:
            try:
                fileWriteln(self,"rotating logfile")
                self.logfile.close()
            except OSError:
                 print ("failed to close logfile")
        if self.path:
            strg = ""
            try:
                t = time.localtime()
                strg = "{:s}_{:d}-{:02d}-{:02d}_{:02d}{:02d}".format(self.path,t[0],t[1],t[2],t[3],t[4])
                self.logfile = open(strg,'a')
                self.length = 0
                self.log (0, "started rotated logfile:" + strg)
            except OSError:
                self.logfile = 0
                print ("failed to open logfile:", strg)



