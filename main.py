import time
import gc
import micropython
import pycom
import machine

from lis2hh   import LIS2HH  
from machine  import I2C
from machine  import SD 
from pytrack  import Pytrack
from config   import conf
from gps      import L76Pos
from log      import Logger
from sigtrack import Messager
from alarm    import GenAlarm

STATE_BANK     = const(0)
CYCLE          = const(1)
POSMESSMINS    = const(2)
LATPACK        = const(3)
LNGPACK        = const(7)
RTC            = const(11)
MAXROLL        = const(15)
MAXPITCH       = const(16)
MAXYAW         = const(17)
MINROLL        = const(18)
MINPITCH       = const(19)
MINYAW         = const(20)
BATTSTATUS     = const(21)
BATTTRIG       = const(22)
PYSENSESTATUS  = const(26)
PYSENSETRIG    = const(27)

# to make it easier to bring up wlan
def wlanup():
    execfile('/flash/wlanup.py')
    for i in range(10):
        if wlan.ifconfig()[0] != '0.0.0.0':
            break
        time.sleep(1)
    print(wlan.ifconfig())

def save(loc, val, size=1):
    global py
    value = val
    for i in range(size):
        try:
            py.write_memory_bank(loc+i, value & 0xFF)
        except OSError as e:
            logger.log(0,"error writing memory location: ",loc+i)
        value = value >> 8

def restore(loc, size=1, signed=False):
    global py
    res=0
    for i in range(size-1,-1,-1):
        res=res*256 + py.read_memory_bank(loc+i)
    if signed and (res > 127): # handle signed 8 bit values
        return(-1*(256-res))
    else:
        return(res)

# before we do anything else

micropython.alloc_emergency_exception_buf(100)
pycom.heartbeat(False)
logger.log(2,"reset, wake causes",machine.reset_cause(),machine.wake_reason())


try:
    sd = SD()
    os.mount(sd, '/sd')
    logfile = conf.LOGFILE
except OSError:
    logfile = 0 # we have no SD for logging


posMessSecs  = conf.POSMSG_FREQ
logger       = Logger(conf.LOGLEVEL, logfile)
battAlarm    = GenAlarm()
pysenseAlarm = GenAlarm()
wdt          = machine.WDT(timeout=conf.WDT_TIMEOUT)
wdt.feed() 

try:
    py  = Pytrack()
    acc = LIS2HH(py)
    gps = L76Pos(py)
    sender = Messager(gps, acc, battAlarm, logger)
    pysenseAlarm.clear()
except Exception as e:
    sender = Messager(0, 0, battAlarm, logger)
    logger.log(0,"pytrack board not detected" + str(e))
    sender.msg_SWalarm(sender.SW_PYSENSE,1)
    time.sleep(60)
    machine.reset()


try:
    wakeup=(restore(STATE_BANK,size=1) == 1)
    save(STATE_BANK, 0, size=1)
    logger.log(1,"wakeup = ", wakeup, restore(STATE_BANK,size=1))

    if wakeup:
        pycom.rgbled(conf.CYCLE_LED)
        cycle = restore(CYCLE)
        posMessSecs = restore(POSMESSMINS)*60
        acc.maxRoll = restore(MAXROLL,signed=True)
        acc.maxPitch = restore(MAXPITCH,signed=True)
        acc.maxYaw = restore(MAXYAW,signed=True)
        acc.minRoll = restore(MINROLL,signed=True)
        acc.minPitch = restore(MINPITCH,signed=True)
        acc.minYaw = restore(MINYAW,signed=True)
        gps.latPack = restore(LATPACK,size=4).to_bytes(4)
        gps.lngPack = restore(LNGPACK,size=4).to_bytes(4)
        epochsecs = restore(RTC,size=4)+posMessSecs
        currDateTime = time.gmtime(epochsecs)
        gps.rtc.init(datetime=currDateTime[:6])
        battAlarm.status = restore(BATTSTATUS)
        battAlarm.trig_secs = restore(BATTTRIG,size=4)
        pysenseAlarm.status = restore(PYSENSESTATUS)
        pysenseAlarm.trig_secs = restore(PYSENSETRIG,size=4)
        logger.log(2,"restored values: ",cycle,posMessSecs,epochsecs,gps.latPack,gps.lngPack,acc.maxRoll,acc.maxPitch,acc.maxYaw,acc.minRoll,acc.minPitch,acc.minYaw)
    else:
        currDateTime = time.gmtime(time.time())
        pycom.rgbled(conf.START_LED)
        cycle = -1
        batt = int(py.read_battery_voltage()/conf.BATT_MAX*255+0.5)
        sender.msg_startup(batt)
except OSError as e:
    logger.log(0,"pysense error during wakeup" + str(e))
    sender.msg_SWalarm(sender.SW_PYSENSE,2)
    try: # try to deep sleep - hofefully next time we're okay
        py.setup_sleep(posMessSecs)
        py.go_to_sleep()
    except Exception as e: # only other option is to reset
        machine.reset()
    



wdt.feed() 
cycle += 1

battfloat=0.0
try:
    battfloat=py.read_battery_voltage()
except OSError as e:
    logger.log(1,"error reading battery")
batt = int(battfloat/conf.BATT_MAX*255+0.5)
logger.log(2,"battery", battfloat, batt)

acc.read()
if wakeup:
    gps.position(conf.MAX_LOCK_TRYS,conf.LOCK_TRY_WAIT)
else:
    gps.position(conf.START_LOCK_TRYS,conf.LOCK_TRY_WAIT)

wdt.feed() 
pycom.rgbled(0x000000)

logger.log(1,gps.lastMessage)

if batt < conf.BATT_CRITICAL:
    battAlarm.trigger()
if batt > conf.BATT_CHARGED:
    battAlarm.clear()

sender.msg_cycle(cycle,batt)

wdt.feed() 
if (cycle%6 == 0) or (cycle == 7): # normally once every 6 cycles but we retry once if not acked
    response = sender.msg_heartbeat(cycle, posMessSecs)
    logger.log(0,"response: " + str(response) + " " + str(sender.sigfox.id()))
    if response:
        cycle=0
        acc.maxRoll  = acc.roll_val
        acc.minRoll  = acc.roll_val
        acc.maxPitch = acc.pitch_val
        acc.minPitch = acc.pitch_val
        acc.maxYaw   = acc.yaw_val
        acc.minYaw   = acc.yaw_val
        if (response[0:4] == b'\0\0\0\0') or (response[0:4] == sender.sigfox.id()):
            posMessSecs = response[4]*60
            logger.log(0, 'new cycle time:', posMessSecs)
                
if battAlarm.ack_trigger() or battAlarm.ack_clear():
    logger.log(0, "process battery alarm", battAlarm.status)
    sender.msg_batteryalarm(batt)

if pysenseAlarm.ack_clear():
    logger.log(0, "pysense okay again", battAlarm.status)
    sender.msg_SWalarm(senser.SW_PYSENSE, pysenseAlarm.last_dur)
    
logger.log(2,"pos result - lat,lng,valid,trys,time", gps.lat,gps.lng,gps.valid,gps.trys,gps.timesync)
logger.log(2,"R,P,Y", acc.roll_val, acc.pitch_val, acc.yaw_val)
logger.log(2,"Battery is", battfloat)
    
gc.collect()
logger.log(2,"saving values",cycle,posMessSecs,time.time(),gps.latPack,gps.lngPack,acc.maxRoll,acc.maxPitch,acc.maxYaw,acc.minRoll,acc.minPitch,acc.minYaw)
wdt.feed() 
try: 
    save(CYCLE,cycle)
    save(POSMESSMINS,posMessSecs//60)
    save(LATPACK,int.from_bytes(gps.latPack),size=4)
    save(LNGPACK,int.from_bytes(gps.lngPack),size=4)
    save(MAXROLL,acc.maxRoll)
    save(MAXPITCH,acc.maxPitch)
    save(MAXYAW,acc.maxYaw)
    save(MINROLL,acc.minRoll)
    save(MINPITCH,acc.minPitch)
    save(MINYAW,acc.minYaw)
    save(BATTSTATUS,battAlarm.status)
    save(BATTTRIG,battAlarm.trig_secs,size=4)
    save(PYSENSESTATUS,pysenseAlarm.status)
    save(PYSENSETRIG,pysenseAlarm.trig_secs,size=4)
except OSError as e:
    pysenseAlarm.trigger()

now = time.time()
if posMessSecs < conf.MIN_POSMSG_FREQ:
    posMessSecs = conf.MIN_POSMSG_FREQ
sleepSecs = posMessSecs-(now % posMessSecs)
if sleepSecs<60:
    sleepSecs=conf.MIN_POSMSG_FREQ 
logger.log(1,"sleeping for",sleepSecs, now, posMessSecs)
logger.flush()
try:
    save(RTC,time.time(),size=4)
    save(STATE_BANK,1) # we now have valid values stored -
except OSError as e:
    sender.msg_SWalarm(sender.SW_PYSENSE,3)

wdt.feed() 
try:
    py.setup_sleep(sleepSecs)
    py.go_to_sleep()
except Exception as e:
    # couldn't sleep so a reset is the only option
    sender.msg_SWalarm(sender.SW_PYSENSE,4)
    machine.reset()

logger.log(2,'Finished deep sleep') # should never get here
