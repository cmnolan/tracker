var messTypes = [
  "Null",
  "Start",
  "Heartbeat",
  "Test",
  "Position",
  "Unused5",
  "Unused6",
  "Unused7",
  "BattAlarm",
  "Error"
  ]


function parseGPSData(s) {
    var a = ((s & 0x7FFFFF | 0x800000) * 1.0 / Math.pow(2,23) * Math.pow(2, ((s>>23 & 0xFF) - 127)));
    if (s & 0x80000000) {
        return -a;
    } else {
        return a;
    }
}

function decodeStatus(status) {
    var result = [
            {    
                 key: "trys",
                 value: status>>3
            },
            {
                 key: "lock",
                 value: status&0x01
            },
            {
                 key: "sync",
                 value: (status&0x02)>>1
            },
            {
                 key: "battAlarm",
                 value: (status&0x04)>>2
            }
            ];
    return result;
}
    
function decodePos(mess) {

     var result = [
         {
              key: "batt",
              value: Math.round(parseInt(mess.substring(4,6),16)*5.0/2.55)/100
         },
         {
             key: "cycle",
             value:  parseInt(mess.substring(6,8),16)
         }
         ];
     if (mess.length > 23) {
         var now = new Date().toISOString().split('T');
         var ts = now[0]+'-'+now[1].substring(0,5)+' UTC';
         var pos = [
             {    
                 key: "geolocalization",
                 value: 1,
                 geo : {
                     long : parseGPSData(parseInt(mess.substring(8,16),16)) ,
                     lat : parseGPSData(parseInt(mess.substring(16,24),16))
                 }
             },
             {
                 key: "lastseen",
                 value: ts
             }
             ];
         result = result.concat(pos);
     }
      
     return result;
}

function decodeStart(mess) {
    var result = [
        {
             key: "batt",
             value: Math.round(parseInt(mess.substring(4,6),16)*5.0/2.55)/100
        },
        { 
             key: "HWver",
             value: parseInt(mess.substring(6,8),16)
        },
        { 
             key: "SWver",
             value: parseInt(mess.substring(8,10),16)
        }
        ]
    return result;
}

function signedByte(str) {
    var result = parseInt(str,16);
    if (result > 127) {
        result = result-256;
    }
    return result;
}

function decodeHeartbeat(mess) {
    var minRoll = signedByte(mess.substring(6,8));
    var minPitch = signedByte(mess.substring(8,10));
    var minYaw = signedByte(mess.substring(10,12));
    var maxRoll = signedByte(mess.substring(12,14));
    var maxPitch = signedByte(mess.substring(14,16));
    var maxYaw = signedByte(mess.substring(16,18));
    var moved = 0;
    if ((maxRoll-minRoll)+(maxPitch-minPitch)+(maxYaw-minYaw) > 12) {
        var moved = 1
    }

    var result = [
        {
             key: "cycle",
             value:  parseInt(mess.substring(2,4),16)
        },
        {
             key: "cycleDuration",
             value: parseInt(mess.substring(4,6),16)
        },
        { 
             key: "minRoll",
             value: minRoll
        },
        { 
             key: "minPitch",
             value: minPitch
        },
        { 
             key: "minYaw",
             value: minYaw
        },
        { 
             key: "maxRoll",
             value: maxRoll
        },
        { 
             key: "maxPitch",
             value: maxPitch
        },
        { 
             key: "maxYaw",
             value: maxYaw
        },
        {
             key: "moved",
             value: moved
        }
        ]
    return result;
}


function decodeBattAlarm(mess) {
    var result = [
        {
             key: "batt",
             value: Math.round(parseInt(mess.substring(4,6),16)*5.0/2.55)/100
        },
        { 
             key: "AlarmDuration",
             value: parseInt(mess.substring(6,14),16)
        }
        ]
    return result;
}

function decodeSWAlarm(mess) {
    var result = [
        {
             key: "batt", // always 0 ATM
             value: Math.round(parseInt(mess.substring(4,6),16)*5.0/2.55)/100
        },
        { 
             key: "SWAlarmType",
             value: parseInt(mess.substring(6,8),16)
        },
        { 
             key: "val1",
             value: parseInt(mess.substring(8,16),16)
        },
        { 
             key: "val2",
             value: parseInt(mess.substring(16,24),16)
        }
        ]
    return result;
}

function main(params, callback){
    var type = parseInt(params.data.substring(0,2),16)
    var messType = [
        {
            key: "type",
            value: messTypes[type]
        }
        ]
    var header = messType.concat(decodeStatus(parseInt(params.data.substring(2,4),16)))
    if (type == 1) {
        var result = header.concat(decodeStart(params.data));
    } else if ((type == 2) || (type == 3)) {
        var result = messType.concat(decodeHeartbeat(params.data));
    } else if (type == 4) {
        var result = header.concat(decodePos(params.data));
    } else if (type == 8) {
        var result = header.concat(decodeBattAlarm(params.data));
    } else if (type == 9) {
        var result = header.concat(decodeSWAlarm(params.data));
    } else {
        var result = header
    }
  
    callback(null, result)
}




