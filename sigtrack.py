# Copyright Linde 2017
# Author: Paul Stanley

import socket
import machine
import binascii

from network import Sigfox
from config  import conf


class Messager:

    # primary message types
    STARTUP        = 1       # power/reset message with some basic info
    HEARTBEAT      = 2       # daily message with some stats
    TESTMSG        = 3       # like a HEARTBEAT but daily counters not reset
    POSMSG         = 4       # regular position message     
    UNUSED1        = 5       # currently not implemented
    UNUSED2        = 6       # currently not implemented
    UNUSED3        = 7       # currently not implemented
    BATTERYALARM   = 8       # Battery voltage critical or restored
    SWALARM        = 9       # S/W issue - e.g. memory leak

    # S/W message subtypes
    SW_STOPPED     = 1       # program stopped by user
    SW_MEMLOW      = 2       # heap memory low => memory leak, val1 = status, val2 = free memory
    SW_LOGERR      = 3       # problem writing to log file, val1 = status
    SW_REBOOT      = 4       # reboot requested
    SW_PYSENSE     = 5       # Problem with pysense board


    def __init__(self, gps, acc, battAlarm, logger):
        self.gps        = gps
        self.acc        = acc
        self.battAlarm  = battAlarm
        self.logger     = logger
        self.sigfox     = Sigfox(mode=Sigfox.SIGFOX, rcz=Sigfox.RCZ1)
        self.sigsoc     = socket.socket(socket.AF_SIGFOX, socket.SOCK_RAW)
        self.sigsoc.setblocking(True)
        self.sigsoc.setsockopt(socket.SOL_SIGFOX, socket.SO_RX, False)
        sigfoxid = "sigfox ID: " + str(binascii.hexlify(self.sigfox.id()))
        sigfoxid = sigfoxid + " PAC: " + str(binascii.hexlify(self.sigfox.pac()))
        logger.log (0,sigfoxid)

    def sendmsg(self, msg):
        msglen = len(msg)
        try:
            sent = self.sigsoc.send(msg)
        except OSError as err:
            sent = 0
            self.logger.log (0,"ERROR - OSError sending message", err.errno)

        if (sent == msglen):
            self.logger.logmess (1,"message sent:", msg)
            return True
        else:
            self.logger.logmess (0,"ERROR failed to send:", msg)
            return False

    def sendrecmsg(self, msg):
        received = 0
        self.sigsoc.setsockopt(socket.SOL_SIGFOX, socket.SO_RX, True)
        if self.sendmsg(msg):
            try:
                received = self.sigsoc.recv(32)
            except OSError as err:
                received = 0
                self.logger.log (0,"ERROR - OSError receiving message", err.errno)
        self.sigsoc.setsockopt(socket.SOL_SIGFOX, socket.SO_RX, False)
        return received


    def statByte(self):
        stat_byte = (self.gps.valid               |     # GPS is valid
                    (self.gps.timesync      << 1) |     # Got a time sync
                    (self.battAlarm.status  << 2) )     # Battery low
        return stat_byte

    def msg_startup(self,batt,hwvers=0):
    # build the startup message
        print(machine.reset_cause())
        msg_bytes = bytes([
                       self.STARTUP,
                       self.statByte(),             # see above
                       batt,                        # 8 bit battery voltage: conf.MAXBATT = 255
                       hwvers,                      # H/W version if available       
                       conf.VERSION,                # S/W version
                       0 #machine.reset_cause()        # pycom's reset cause
                         ])
        self.sendmsg(msg_bytes)

    # the test message is the same content as heartbeat just a different trigger/type
    def msg_heartbeat_test(self, msgtype, cycleNo, posMessSecs):
        if cycleNo > 255:
            cycleNo = 255
        print( self.acc.minRoll, self.acc.minPitch,    self.acc.minYaw, self.acc.maxRoll, self.acc.maxPitch, self.acc.maxYaw )
        # build the heartbeat / test message
        msg_bytes = bytes([                    # all cumulatives are reset on ack
                       msgtype,                # HEARTBEAT or TESTMSG
                       cycleNo,                # cycle no. - reset when we get an ack
                       posMessSecs//60,        # current cycle duration in minutes
                       self.acc.minRoll & 0xFF,       # since last sync     
                       self.acc.minPitch & 0xFF,      # since last sync     
                       self.acc.minYaw & 0xFF ,       # since last sync     
                       self.acc.maxRoll & 0xFF ,      # since last sync     
                       self.acc.maxPitch & 0xFF ,     # since last sync     
                       self.acc.maxYaw  & 0xFF   ,    # since last sync     
                         ])
        if msgtype == self.HEARTBEAT:
            return self.sendrecmsg(msg_bytes) # send and get an ack, with time to resync
        else:                                 # test message won't be acked
            self.sendmsg(msg_bytes)

    def msg_heartbeat(self, cycleNo, posMessSecs):
        return self.msg_heartbeat_test(self.HEARTBEAT, cycleNo, posMessSecs)

    def msg_test(self, cycleNo, posMessSecs):
        self.msg_heartbeat_test(self.TESTMSG, cycleNo, posMessSecs)

    def msg_cycle(self, cycleNo, batt):
        if cycleNo > 255:
            cycleNo = 255
        tryrange  = round((self.gps.trys/conf.MAX_LOCK_TRYS) * 30)
        if (tryrange < 32):
            statbyte = (tryrange << 3) + self.statByte()
        else:
            statbyte = (31<<3) + self.statByte()
        # build the regular position message
        if self.gps.valid:
            msg_bytes = bytes([                             
                       self.POSMSG,        # regular cyclic position
                       statbyte,           # trys + Status
                       batt,               # 8 bit battery voltage: conf.MAXBATT = 255
                       cycleNo,            # cycle no. - reset when we get an ack
                       self.gps.lngPack[3],# last valid longitude
                       self.gps.lngPack[2],# 
                       self.gps.lngPack[1],# ... 4 bytes ...
                       self.gps.lngPack[0],# ... 32 bit float
                       self.gps.latPack[3],# last valid latitude 
                       self.gps.latPack[2],# 
                       self.gps.latPack[1],# ... 4 bytes ...
                       self.gps.latPack[0] # ... 32 bit float
                         ])
        else:
            print (self.POSMSG,self.statByte(), batt,cycleNo)
            msg_bytes = bytes([
                       self.POSMSG,       # regular cyclic position
                       self.statByte(),   # Status
                       batt,              # 8 bit battery voltage: conf.MAXBATT = 255
                       cycleNo,           # cycle no. - reset when we get an ack
                         ])
           
        self.sendmsg(msg_bytes)

    def msg_batteryalarm(self, batt):
        msg_bytes = bytes([
                       self.BATTERYALARM,
                       self.statByte(),                 # status byte - see above
                       batt,                            # 8 bit battery voltage: conf.MAXBATT = 255
                       self.battAlarm.last_dur >> 24,   # clear -> alarm duration in secs
                       (self.battAlarm.last_dur & 0x00FFFFFF) >> 16,
                       (self.battAlarm.last_dur & 0x0000FFFF) >> 8,
                       (self.battAlarm.last_dur & 0x000000FF)
                         ])
        self.sendmsg(msg_bytes)

    def msg_SWalarm(self, subtype, val1=0, val2=0):
        msg_bytes = bytes([
                       self.SWALARM,
                       0,                               # leave these as 0 as some SW errors
                       0,                               # ...will result in them not being available
                       subtype,                         # SW_PYSENSE | .........
                       val1 >> 24,                      # 2 optional 32 bit values
                       (val1 & 0x00FFFFFF) >> 16,
                       (val1 & 0x0000FFFF) >> 8,
                       (val1 & 0x000000FF),
                       val2 >> 24,
                       (val2 & 0x00FFFFFF) >> 16,
                       (val2 & 0x0000FFFF) >> 8,
                       (val2 & 0x000000FF),
                         ])
        self.sendmsg(msg_bytes)



