# reset with wlan up        
import os
import machine
import network

from network import WLAN
from machine import UART

uart = UART(0, 115200)
os.dupterm(uart)

server = network.Server()
server.deinit() # disable the server
server.init(login=('micro', 'btf987'), timeout=6000)

wlan = WLAN() # get current object, without changing the mode

if machine.reset_cause() != machine.SOFT_RESET:
    wlan.init(mode=WLAN.STA)
    wlan.ifconfig(config=('dhcp'))

if not wlan.isconnected():
    wlan.connect('Transfer', auth=(WLAN.WPA2, 'b2tf1916'), timeout=5000)
    # don't wait for wifi
